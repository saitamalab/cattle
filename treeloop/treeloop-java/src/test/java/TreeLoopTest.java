import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class TreeLoopTest {

    @Test
    public void testNonLoop() {
        Map<String, String> m1 = new HashMap<String, String>();
        m1.put("Root", "{\"tag\":\"Root\",\"name\":\"Root\",\"y\":\"R001\",\"n\":\"R001\"}");
        m1.put("R001", "{\"tag\":\"R001\",\"name\":\"R001\",\"y\":\"TReject\",\"n\":\"R002\"}");
        m1.put("R002", "{\"tag\":\"R002\",\"name\":\"R002\",\"y\":\"TReject\",\"n\":\"R003\"}");
        m1.put("R003", "{\"tag\":\"R003\",\"name\":\"R003\",\"y\":\"TReject\",\"n\":\"R004\"}");
        m1.put("R004", "{\"tag\":\"R004\",\"name\":\"R004\",\"y\":\"TReject\",\"n\":\"TAccept\"}");
        m1.put("TReject", "{\"tag\":\"TReject\",\"name\":\"TReject\",\"y\":\"\",\"n\":\"\"}");
        m1.put("TAccept", "{\"tag\":\"TAccept\",\"name\":\"TAccept\",\"y\":\"\",\"n\":\"\"}");
        Node node = TreeLoop.buildTree(m1);
        Assert.assertNotNull(node);
    }

    @Test
    public void testLoop() {
        Map<String, String> m1 = new HashMap<String, String>();
        m1.put("Root", "{\"tag\":\"Root\",\"name\":\"Root\",\"y\":\"R001\",\"n\":\"R002\"}");
        m1.put("R001", "{\"tag\":\"R001\",\"name\":\"R001\",\"y\":\"R003\",\"n\":\"R004\"}");
        m1.put("R002", "{\"tag\":\"R002\",\"name\":\"R002\",\"y\":\"R005\",\"n\":\"R006\"}");
        m1.put("R003", "{\"tag\":\"R003\",\"name\":\"R003\",\"y\":\"TAccept\",\"n\":\"TReject\"}");
        m1.put("R004", "{\"tag\":\"R004\",\"name\":\"R004\",\"y\":\"TAccept\",\"n\":\"R002\"}");
        m1.put("R005", "{\"tag\":\"R005\",\"name\":\"R005\",\"y\":\"R001\",\"n\":\"TReject\"}");
        m1.put("R006", "{\"tag\":\"R006\",\"name\":\"R006\",\"y\":\"TAccept\",\"n\":\"TReject\"}");
        m1.put("TReject", "{\"tag\":\"TReject\",\"name\":\"TReject\",\"y\":\"\",\"n\":\"\"}");
        m1.put("TAccept", "{\"tag\":\"TAccept\",\"name\":\"TAccept\",\"y\":\"\",\"n\":\"\"}");
        Node node = TreeLoop.buildTree(m1);
        Assert.assertNull(node);
    }
}
