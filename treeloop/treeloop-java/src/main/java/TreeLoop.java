import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

class TreeLoop {
    private final static String TROOT = "Root";

    static Node buildTree(Map<String, String> m) {
        Node root = newNode(TROOT, m);
        if (root == null) {
            System.out.println("root node not found");
            return null;
        }
        boolean success = root.grow(new LinkedHashSet<String>(), m);
        if (!success) {
            return null;
        }
        return root;
    }

    static Node newNode(String rid, Map<String, String> m) {
        if (m == null) {
            System.out.println(rid + " node not found");
            return null;
        }
        String jsonStr = m.get(rid);
        if (jsonStr == null) {
            return null;
        }
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, Node.class);
    }
}

class Node {
    private String tag;
    private String name;
    private String y;
    private String n;
    private Node yNode;
    private Node nNode;

    @Override
    public String toString() {
        return "Node{" +
                "tag='" + tag + '\'' +
                ", name='" + name + '\'' +
                ", y='" + y + '\'' +
                ", n='" + n + '\'' +
                ", yNode=" + yNode +
                ", nNode=" + nNode +
                '}';
    }

    public Node getyNode() {
        return yNode;
    }

    private void setyNode(Node yNode) {
        this.yNode = yNode;
    }

    public Node getnNode() {
        return nNode;
    }

    private void setnNode(Node nNode) {
        this.nNode = nNode;
    }

    public String getTag() {
        return tag;
    }

    public String getName() {
        return name;
    }


    void setTag(String tag) {
        this.tag = tag;
    }

    void setName(String name) {
        this.name = name;
    }


    String getY() {
        return y;
    }

    String getN() {
        return n;
    }


    Boolean grow(Set<String> nSet, Map<String, String> m) {
        if (nSet.contains(this.tag)) {
            System.out.println("loop found in tree, node path:" + nSet + "\tcurNode:" + this.getTag());
            return false;
        }
        Set<String> nodeSet = new LinkedHashSet<String>(nSet);
        nodeSet.add(this.tag);
        if (StringUtils.isNotBlank(this.getY())) {
            Node y = TreeLoop.newNode(this.getY(), m);
            if (y == null) {
                return false;
            }
            this.setyNode(y);
            if (!y.grow(nodeSet, m)) {
                return false;
            }
        }
        if (StringUtils.isNotBlank(this.getN())) {
            Node n = TreeLoop.newNode(this.getN(), m);
            if (n == null) {
                return false;
            }
            this.setnNode(n);
            return n.grow(nodeSet, m);
        }
        return true;
    }

}
