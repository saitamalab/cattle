package treeloop

import "fmt"

type Rule struct {
	Y string `json:"y" binding:"required"`
	N string `json:"n" binding:"required"`
}

// Node ..
type Node struct {
	Tag  string
	Name string
	Next Branch
	Rule *Rule
}

// Branch ..
type Branch struct {
	Y *Node
	N *Node
}

// NewNode ..
func NewNode(rid string) (*Node, error) {
	return &Node{Tag: rid, Name: "rid" + rid}, nil
}

// Grow builds a tree
func (n *Node) Grow(count, maxCount int) error {
	count += 1
	if count > maxCount {
		return fmt.Errorf("loop found in tree, count %d, max count %d", count, maxCount)
	}

	if n.Rule.Y != "" {
		node, err := NewNode(n.Rule.Y)
		if err != nil {
			return fmt.Errorf("grow %s fail, %v", n.Tag, err)
		}
		n.Next.Y = node
		if err := node.Grow(count, maxCount); err != nil {
			return err
		}
	}
	if n.Rule.N != "" {
		node, err := NewNode(n.Rule.N)
		if err != nil {
			return fmt.Errorf("grow %s fail, %v", n.Tag, err)
		}
		n.Next.N = node
		if err := node.Grow(count, maxCount); err != nil {
			return err
		}
	}

	return nil
}

const (
	TRoot = "Root"
)

// Tree represents rule tree
type Tree struct {
	Node
}

func BuildTree() (*Tree, error) {
	rootNode, err := NewNode(TRoot)
	if err != nil {
		return nil, err
	}
	if err := rootNode.Grow(0, 100); err != nil {
		return nil, err
	}
	tree := &Tree{Node: *rootNode}
	return tree, nil
}
