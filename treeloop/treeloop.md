
## 项目中现有一下代码（略做了简化）

```
type Rule struct {
	Y            string `json:"y" binding:"required"`
	N            string `json:"n" binding:"required"`
}
// Node ..
type Node struct {
	Tag     string
	Name    string
	Next    Branch
	Rule    *Rule
}


// Branch ..
type Branch struct {
	Y *Node
	N *Node
}

// NewNode ..
func NewNode(rid string) (*Node, error) {
	return &Node{Tag: rid,Name:"rid"+rid},nil
}

// Grow builds a tree
func (n *Node) Grow(count, maxCount int) error {
	count += 1
	if count > maxCount {
		return fmt.Errorf("loop found in tree, count %d, max count %d", count, maxCount)
	}

	if n.Rule.Y != "" {
		node, err := NewNode(n.Rule.Y)
		if err != nil {
			return fmt.Errorf("grow %s fail, %v", n.Tag, err)
		}
		n.Next.Y = node
		if err := node.Grow(count, maxCount); err != nil {
			return err
		}
	}
	if n.Rule.N != "" {
		node, err := NewNode(n.Rule.N)
		if err != nil {
			return fmt.Errorf("grow %s fail, %v", n.Tag, err)
		}
		n.Next.N = node
		if err := node.Grow(count, maxCount); err != nil {
			return err
		}
	}

	return nil
}

const (
	TRoot   = "Root"
)

// Tree represents rule tree
type Tree struct {
	Node
}

func BuildTree() (*Tree, error) {
	rootNode, err := NewNode(TRoot)
 	if err != nil {
		return nil, err
	}
	if err := rootNode.Grow(0, 100); err != nil {
		return nil, err
	}
	tree := &Tree{Node: *rootNode}
	return tree, nil
}
```
### 问题概要
在构建Root为根节点的树时，每个节点均会指向Y节点和N节 （除了叶节点），因此需要判断是否有环的存在，以免在程序运行时进入死循环。
目前判断是否有环的方法请看以下代码

```
func BuildTree() (*Tree, error) {
    ...
	if err := rootNode.Grow(0, 100); err != nil {
		return nil, err
	}
	...
	}
func (n *Node) Grow(count, maxCount int) error {
	count += 1
	if count > maxCount {
		return fmt.Errorf("loop found in tree, count %d, max count %d", count, maxCount)
	}
	...
	}
```
简单暴力地认为NewNode方法执行超过100次，即有环。
现在需要一种更为优雅的方法来判断树是否有环