package main

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"time"
)

var maxID int

type Message struct {
	Body      string `json:"body"`
	Timestamp string `json:"timestamp"`
}

func (m *Message) String() string {
	return m.Timestamp + " : " + m.Body
}

type User struct {
	ID       int
	conn     *websocket.Conn
	msg      chan *Message
	doneChan chan bool
	Server   *Server
}

func (u *User) wmsg(msg *Message) {
	log.Println("write to user", u.ID, msg)
	u.msg <- msg
	log.Println("write to user finished", u.ID, msg)
}

func (u *User) Listen() {
	log.Println("user Listening .....")
	go u.listenWrite()
	u.listenRead()
}
func (u *User) listenRead() {
	for {
		select {
		case <-u.doneChan:
			u.Server.removeUser <- u
		default:
			_, s, err := u.conn.ReadMessage()
			if err != nil {
				log.Println("Error while reading JSON from websocket ", err.Error())
				u.doneChan <- true
			}
			log.Println("user read ", string(s))
		}
	}
}
func (u *User) listenWrite() {
	log.Println("user Write .....")
	for msg := range u.msg {
		err := u.conn.WriteJSON(msg)
		log.Println("user Write .....", msg)
		if err != nil {
			log.Println("user Write err .....", err)
			u.doneChan <- true
		}
	}
}

type Server struct {
	users      map[int]*User
	addUser    chan *User
	removeUser chan *User
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func NewServer() *Server {
	users := map[int]*User{}
	adduser := make(chan *User)
	removeuser := make(chan *User)

	return &Server{
		users,
		adduser,
		removeuser,
	}
}

func (s *Server) AddUser(user *User) {
	s.addUser <- user
}

func main() {
	s := NewServer()
	go s.Listen()
	go s.hello()
	http.ListenAndServe(":9999", nil)
}

func (s *Server) handle(responseWriter http.ResponseWriter, request *http.Request) {
	conn, err := upgrader.Upgrade(responseWriter, request, nil)
	if err != nil {
		log.Println(err)
	}
	maxID++
	user := &User{
		ID:     maxID,
		conn:   conn,
		msg:    make(chan *Message, 100),
		Server: s,
	}
	s.AddUser(user)
	log.Println("user added successfully")
	user.Listen()
}

func (s *Server) Listen() {
	log.Println("Server Listening .....")
	http.HandleFunc("/wb", s.handle)

	for {
		select {
		// Adding a new user
		case user := <-s.addUser:
			s.users[user.ID] = user
		case u := <-s.removeUser:
			log.Println("remove user .....", u.ID)
			delete(s.users, u.ID)
		}
	}
}

// say hello to client
func (s *Server) hello() {
	for {
		log.Println("users count ", len(s.users))
		for _, user := range s.users {
			t := time.Now().Format("2006-01-02 15:04:05")
			msg := Message{
				Body:      "hello client",
				Timestamp: t,
			}
			user.wmsg(&msg)
		}
		log.Println("write to users finished ", len(s.users))
		time.Sleep(10 * time.Second)
	}
}
